=== Gital Display Ads ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.6
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Display Ads is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Description ==
The Gital Display Ads is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Installation ==
Simply activate the plugin.

To override the aspect ratios of the banners, add the following constants to wp-config:

Bedrock:

// Override the aspect ratios of the display ads
Config::define( 'G_DA_AR_MOBILE', '1:1' );
Config::define( 'G_DA_AR_DESKTOP', '79:640' );

Vanilla:

// Override the aspect ratios of the display ads
define( 'G_DA_AR_MOBILE', '1:1' );
define( 'G_DA_AR_DESKTOP', '79:640' );

== Changelog ==

= 1.9.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 1.8.0 - 2023.08.20 = 
* Update: Added "Ad" label to the ads.

= 1.7.0 - 2023.05.29 = 
* Update: The image for the desktop version was to small in wp-admin.

= 1.6.0 - 2023.05.29 = 
* Update: Added gif support.

= 1.5.0 - 2023.04.24 = 
* Update: The ads won't render if the url contains "iframe". This is to prevent issues with BeBuilder.

= 1.4.0 - 2023.03.29 = 
* Update: Gibon Webb Uppsala is now Gibon Webb.

= 1.3.2 - 2023.03.07 = 
* Changed the classes of the columns in wp-admin.

= 1.2.2 - 2023.02.21 = 
* Changed the name om the pulse animation.
* The ads wrapper is now removed if there are no ads.

= 1.2.1 - 2023.02.16 = 
* Production.

= 1.0.9 - 2023.02.14 = 
* Update: Init.