<?php
/**
 * Plugin Name: Gital Display Ads
 * Author: Gibon Webb
 * Version: 1.9.0
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Display Ads is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.
 *
 * @package Gital Display Ads
 */

namespace gital_display_ads;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-display-ads', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_display_ads\textdomain' );

// Constants.
if ( ! defined( 'G_DA_ROOT' ) ) {
	define( 'G_DA_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_DA_ASSETS' ) ) {
	define( 'G_DA_ASSETS', G_DA_ROOT . '/assets' );
}
if ( ! defined( 'G_DA_RESOURSES' ) ) {
	define( 'G_DA_RESOURSES', G_DA_ROOT . '/assets/resources' );
}
if ( ! defined( 'G_DA_ROOT_PATH' ) ) {
	define( 'G_DA_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_DA_ASSETS_PATH' ) ) {
	define( 'G_DA_ASSETS_PATH', G_DA_ROOT_PATH . 'assets/' );
}
if ( ! defined( 'G_DA_FUNCTIONS_PATH' ) ) {
	define( 'G_DA_FUNCTIONS_PATH', G_DA_ROOT_PATH . 'functions/' );
}
if ( ! defined( 'G_DA_CLASSES_PATH' ) ) {
	define( 'G_DA_CLASSES_PATH', G_DA_ROOT_PATH . 'classes/' );
}
if ( defined( 'G_DA_AR_DESKTOP' ) && ! empty( G_DA_AR_DESKTOP ) ) {
	define( 'G_DA_AR_DESKTOP_CLEAN', 'calc(' . str_replace( ':', ' / ', G_DA_AR_DESKTOP ) . ')' );
}
if ( defined( 'G_DA_AR_MOBILE' ) && ! empty( G_DA_AR_MOBILE ) ) {
	define( 'G_DA_AR_MOBILE_CLEAN', 'calc(' . str_replace( ':', ' / ', G_DA_AR_MOBILE ) . ')' );
}

// Init the updater.
require G_DA_ROOT_PATH . 'vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-display-ads.json',
	__FILE__,
	'gital-display-ads'
);

/**
 * Enquene public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function assets() {
	wp_register_style( 'g_da_style', G_DA_ASSETS . '/styles/gital.display.ads.min.css', array(), '1.0.1' );
	wp_enqueue_style( 'g_da_style' );
	if ( defined( 'G_DA_AR_DESKTOP_CLEAN' ) && ! empty( G_DA_AR_DESKTOP_CLEAN ) ) {
		wp_add_inline_style( 'g_da_admin_style', ':root{--g-da-ar-desktop:' . G_DA_AR_DESKTOP_CLEAN . '};' );
	}
	if ( defined( 'G_DA_AR_MOBILE_CLEAN' ) && ! empty( G_DA_AR_MOBILE_CLEAN ) ) {
		wp_add_inline_style( 'g_da_admin_style', ':root{--g-da-ar-mobile:' . G_DA_AR_MOBILE_CLEAN . '};' );
	}

	wp_register_script( 'g_da_script', G_DA_ASSETS . '/scripts/gital.display.ads.min.js', array(), '1.1.0', true );
	wp_enqueue_script( 'g_da_script' );
}
add_action( 'wp_enqueue_scripts', 'gital_display_ads\assets' );

// Post types.
require_once G_DA_CLASSES_PATH . 'class-post-type.php';
new Post_Type();

// Rest routes.
require_once G_DA_CLASSES_PATH . 'class-rest-routes.php';
new Rest_Routes();
