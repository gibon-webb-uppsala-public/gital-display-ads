import { shuffle } from 'lodash';

const render_display_ads = (
	display_ads: Array<number>,
	display_ads_wrapper: Element
) => {
	const randomized_ads = shuffle(display_ads);
	let index = 0;
	render_display_ad(randomized_ads[index], display_ads_wrapper);
	setInterval(() => {
		index++;
		if (randomized_ads.length === index) {
			index = 0;
		}
		render_display_ad(display_ads[index], display_ads_wrapper);
	}, 5000);
};

const render_display_ad = async (id: number, display_ads_wrapper: Element) => {
	fetch_display_ad(id).then((response) => {
		if (response) {
			display_ads_wrapper.innerHTML = response;
		}
	});
};

const fetch_display_ad = async (id: number) => {
	const response = await fetch('/wp-json/gital/display_ads/fetch_ad', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			id,
		}),
	});
	return response.json();
};

const fetch_display_ads = async () => {
	const response = await fetch(
		'/wp-json/gital/display_ads/fetch_available_ads',
		{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		}
	);
	return response.json();
};

const display_ads_init = (display_ads_wrapper_id: string) => {
	const display_ads_wrapper = document.getElementById(display_ads_wrapper_id);
	fetch_display_ads().then((response) => {
		if (response && response.length > 0) {
			render_display_ads(response, display_ads_wrapper);
		} else {
			display_ads_wrapper.remove();
		}
	});
};

declare global {
	interface Window {
		g_display_ads_init: any;
	}
}

window.g_display_ads_init = display_ads_init;
