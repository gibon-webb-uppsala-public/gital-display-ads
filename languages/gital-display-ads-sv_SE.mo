��          �       �      �     �     �     �     �     �          %     8     J     ]     o     {     �     �     �     �  �   �  �   W  "   �  "        4  q   G     �     �     �  �  �     �     �     �     �     �     �     �          -     F     `     m     �     �  *   �     �     �  &   �           /     <  q   P     �     �     �   Ad Add New Add New Display ad Add new on toolbarDisplay ad Admin menu textDisplay ads All Display ads Display Ad Desktop Display Ad Mobile Display ad desktop Display ad mobile Display ads Edit Display ad Link New Display ad No display ads found in Trash. No display ads found. Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).Insert into display ad Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).Uploaded to this display ad Post type general nameDisplay ads Post type singular nameDisplay ad Search Display ads The aspect ratio of the image should be %s. If an other aspect ratio will be uploaded, the image will be cropped. The sender's name View Display ad View Display ads Project-Id-Version: Gital Display Ads
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-02-16 10:11+0000
PO-Revision-Date: 2024-08-20 15:08+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.3; wp-6.1.1
X-Domain: gital-display-ads Annons Lägg till ny Lägg till nu bannerannons Bannerannons Bannerannonser Alla Bannerannonser Bannerannons Stor Skärm Bannerannons Liten Skärm Bannerannons stor skärm Bannerannons liten skärm Bannerannons Redigera Bannerannons Länk Ny Bannerannons Inga Bannerannonser funna i papperskorgen. Inga bannerannonser funna. Lägg till i bannerannons Uppladdad till den här bannerannonsen Bannerannonser Bannerannons Sök Bannerannonser Bildförhållandet på bilden skall vara %s. Om ett annat bildförhållande används så kommer bilden beskäras. Avsändarens namn Visa Bannerannons Visa Bannerannonser 