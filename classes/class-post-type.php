<?php
/**
 * Example class
 *
 * @package Gital Library
 */

namespace gital_display_ads;

if ( ! class_exists( 'Post_Type' ) ) {
	/**
	 * Post Type
	 *
	 * Registers and manages the post type for display ads
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.3.0
	 * @since 1.0.0
	 */
	class Post_Type {

		public function __construct() {
			add_action( 'init', array( $this, 'create_display_ad_post_type' ) );
			add_filter( 'enter_title_here', array( $this, 'display_ad_title_placeholder' ), 20, 2 );
			add_filter( 'manage_display_ads_posts_columns', array( $this, 'manage_display_ad_columns' ) );
			add_action( 'manage_display_ads_posts_custom_column', array( $this, 'manage_display_ad_columns_content' ), 10, 2 );
			add_shortcode( 'g_display_ads', array( $this, 'shortcode_handler' ) );
			add_action( 'acf/init', array( $this, 'acf_fields' ) );
			add_action( 'admin_head', array( $this, 'admin_styling' ) );
		}

		/**
		 * Create the post type
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		public function create_display_ad_post_type() {
			register_post_type(
				'display_ads',
				array(
					'labels'        => array(
						'name'                  => _x( 'Display ads', 'Post type general name', 'gital-display-ads' ),
						'singular_name'         => _x( 'Display ad', 'Post type singular name', 'gital-display-ads' ),
						'menu_name'             => _x( 'Display ads', 'Admin menu text', 'gital-display-ads' ),
						'name_admin_bar'        => _x( 'Display ad', 'Add new on toolbar', 'gital-display-ads' ),
						'add_new'               => __( 'Add New', 'gital-display-ads' ),
						'add_new_item'          => __( 'Add New Display ad', 'gital-display-ads' ),
						'new_item'              => __( 'New Display ad', 'gital-display-ads' ),
						'edit_item'             => __( 'Edit Display ad', 'gital-display-ads' ),
						'view_item'             => __( 'View Display ad', 'gital-display-ads' ),
						'view_items'            => __( 'View Display ads', 'gital-display-ads' ),
						'all_items'             => __( 'All Display ads', 'gital-display-ads' ),
						'search_items'          => __( 'Search Display ads', 'gital-display-ads' ),
						'not_found'             => __( 'No display ads found.', 'gital-display-ads' ),
						'not_found_in_trash'    => __( 'No display ads found in Trash.', 'gital-display-ads' ),
						'insert_into_item'      => _x( 'Insert into display ad', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).', 'gital-display-ads' ),
						'uploaded_to_this_item' => _x( 'Uploaded to this display ad', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).', 'gital-display-ads' ),
					),
					'description'   => __( 'Holds our display ads specific data', 'gital-display-ads' ),
					'public'        => false,
					'show_ui'       => true,
					'show_in_menu'  => true,
					'has_archive'   => false,
					'show_in_rest'  => true,
					'menu_icon'     => 'dashicons-align-wide',
					'menu_position' => 5,
					'supports'      => array( 'title' ),
					'hierarchical'  => false,
					'taxonomies'    => array( '' ),
				)
			);
		}

		/**
		 * Manage the display ads title placeholder
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.0.0
		 */
		public function display_ad_title_placeholder( $title, $post ) {
			if ( 'display_ads' === $post->post_type ) {
				$title = __( "The sender's name", 'gital-display-ads' );
				return $title;
			}

			return $title;
		}

		/**
		 * Manages the display ads admin columns
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.0.0
		 */
		public function manage_display_ad_columns( $columns ) {
			$columns = array(
				'cb'           => $columns['cb'],
				'title'        => __( "The sender's name", 'gital-display-ads' ),
				'g_ad_desktop' => __( 'Display ad desktop', 'gital-display-ads' ),
				'g_ad_mobile'  => __( 'Display ad mobile', 'gital-display-ads' ),
			);

			return $columns;
		}

		/**
		 * Manages the display ads admin columns content
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.0.0
		 */
		public function manage_display_ad_columns_content( $column, $post_id ) {
			// Image column.
			if ( 'g_ad_desktop' === $column ) {
				$image = get_field( 'display_ad_desktop' ) ? wp_get_attachment_image_src( get_field( 'display_ad_desktop' ), 'large' ) : false;
				if ( $image ) {
					echo '<img class="g-da-admin-image g-da-admin-image--desktop" src="' . $image[0] . '">';
				}
			}
			if ( 'g_ad_mobile' === $column ) {
				$image = get_field( 'display_ad_mobile' ) ? wp_get_attachment_image_src( get_field( 'display_ad_mobile' ), 'medium' ) : false;
				if ( $image ) {
					echo '<img class="g-da-admin-image g-da-admin-image--mobile" src="' . $image[0] . '">';
				}
			}
		}

		/**
		 * Manages the shortcode
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function shortcode_handler( $display ) {
			if ( strpos( $_SERVER['REQUEST_URI'], 'iframe' ) !== false ) {
				return;
			}
			$display_ads_id = 'g-da-display-ads--' . wp_rand( 1000, 9999 );
			$content        = '<div id="' . esc_attr( $display_ads_id ) . '" class="g-da-display-ads" label="' . __('Ad', 'gital-display-ads' ) . '"><div class="g-da-display-ads__inner g-da-display-ads__inner--pulse">';
			$script         = '<script>window.addEventListener("load", () => { g_display_ads_init("' . $display_ads_id . '") });</script>';
			$content       .= $script . '</div>';
			return $content;
		}

		/**
		 * ACF Fields
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public function acf_fields() {
			if ( function_exists( 'acf_add_local_field_group' ) ) {
				acf_add_local_field_group(
					array(
						'key'                   => 'gital_display_ads_fields',
						'title'                 => __( 'Display ads', 'gital-display-ads' ),
						'fields'                => array(
							array(
								'key'                => 'gital_display_ads_display_ad_desktop',
								'label'              => __( 'Display Ad Desktop', 'gital-display-ads' ),
								'name'               => 'display_ad_desktop',
								'aria-label'         => '',
								'type'               => 'image',
								'instructions'       => sprintf(
									/* translators: %s: The aspect ratio */
									__( 'The aspect ratio of the image should be %s. If an other aspect ratio will be uploaded, the image will be cropped.', 'gital-display-ads' ),
									defined( 'G_DA_AR_MOBILE' ) && ! empty( G_DA_AR_MOBILE ) ? G_DA_AR_MOBILE : '640 : 79'
								),
								'required'           => 0,
								'conditional_logic'  => 0,
								'wrapper'            => array(
									'width' => '50',
									'class' => 'g-da-admin-image-acf g-da-admin-image-acf--desktop',
									'id'    => '',
								),
								'relevanssi_exclude' => 0,
								'return_format'      => 'id',
								'library'            => 'all',
								'min_width'          => '',
								'min_height'         => '',
								'min_size'           => '',
								'max_width'          => 3840,
								'max_height'         => 3840,
								'max_size'           => '',
								'mime_types'         => 'gif,png,svg,jpeg,jpg',
								'preview_size'       => 'medium',
							),
							array(
								'key'                => 'gital_display_ads_display_ad_mobile',
								'label'              => __( 'Display Ad Mobile', 'gital-display-ads' ),
								'name'               => 'display_ad_mobile',
								'aria-label'         => '',
								'type'               => 'image',
								'instructions'       => sprintf(
									/* translators: %s: The aspect ratio */
									__( 'The aspect ratio of the image should be %s. If an other aspect ratio will be uploaded, the image will be cropped.', 'gital-display-ads' ),
									defined( 'G_DA_AR_MOBILE' ) && ! empty( G_DA_AR_MOBILE ) ? G_DA_AR_MOBILE : '1 : 1'
								),
								'required'           => 0,
								'conditional_logic'  => 0,
								'wrapper'            => array(
									'width' => '50',
									'class' => 'g-da-admin-image-acf g-da-admin-image-acf--mobile',
									'id'    => '',
								),
								'relevanssi_exclude' => 0,
								'return_format'      => 'id',
								'library'            => 'all',
								'min_width'          => '',
								'min_height'         => '',
								'min_size'           => '',
								'max_width'          => 3840,
								'max_height'         => 3840,
								'max_size'           => '',
								'mime_types'         => 'gif,png,svg,jpeg,jpg',
								'preview_size'       => 'medium',
							),
							array(
								'key'                => 'gital_display_ads_link',
								'label'              => __( 'Link', 'gital-display-ads' ),
								'name'               => 'link',
								'aria-label'         => '',
								'type'               => 'link',
								'instructions'       => '',
								'required'           => 0,
								'conditional_logic'  => 0,
								'wrapper'            => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'relevanssi_exclude' => 0,
								'return_format'      => 'url',
							),
						),
						'location'              => array(
							array(
								array(
									'param'    => 'post_type',
									'operator' => '==',
									'value'    => 'display_ads',
								),
							),
						),
						'menu_order'            => 0,
						'position'              => 'normal',
						'style'                 => 'default',
						'label_placement'       => 'top',
						'instruction_placement' => 'label',
						'hide_on_screen'        => '',
						'active'                => true,
						'description'           => '',
						'show_in_rest'          => 0,
					)
				);
			};
		}

		/**
		 * Styles the wp-admin area.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 */
		public function admin_styling() {
			wp_register_style( 'g_da_admin_style', '', array(), '1.0' );
			wp_enqueue_style( 'g_da_admin_style' );
			wp_add_inline_style( 'g_da_admin_style', '.g-da-admin-image,.g-da-admin-image-acf img{object-fit:cover;object-position:center center;height:50px!important;border-radius:3px;}.g-da-admin-image--desktop,.g-da-admin-image-acf--desktop img{width:min(calc(50px / var(--g-da-ar-desktop, calc(79 / 640))), 100%) !important}.g-da-admin-image--mobile,.g-da-admin-image-acf--mobile img{width:calc(50px / var(--g-da-ar-mobile, calc(1 / 1)))!important}' );

			if ( defined( 'G_DA_AR_DESKTOP_CLEAN' ) && ! empty( G_DA_AR_DESKTOP_CLEAN ) ) {
				wp_add_inline_style( 'g_da_admin_style', ':root{--g-da-ar-desktop:' . G_DA_AR_DESKTOP_CLEAN . '};' );
			}
			if ( defined( 'G_DA_AR_MOBILE_CLEAN' ) && ! empty( G_DA_AR_MOBILE_CLEAN ) ) {
				wp_add_inline_style( 'g_da_admin_style', ':root{--g-da-ar-mobile:' . G_DA_AR_MOBILE_CLEAN . '};' );
			}
		}
	}
}
