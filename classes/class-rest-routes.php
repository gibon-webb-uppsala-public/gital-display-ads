<?php
/**
 * Popup handler
 *
 * @package Gital Blocks
 */

namespace gital_display_ads;

use function \gital_library\image_src as image_src;
use function \gital_library\clean_classes as clean_classes;

if ( ! class_exists( 'Rest_Routes' ) ) {
	/**
	 * Rest Route
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.1.0
	 */
	class Rest_Routes {
		public function __construct() {
			add_action( 'rest_api_init', array( $this, 'rest_routes' ) );
		}


		/**
		 * Register a new route for the REST API
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function rest_routes() {
			register_rest_route(
				'gital',
				'/display_ads/fetch_ad',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'fetch_ad_callback' ),
					'permission_callback' => '__return_true',
				)
			);
			register_rest_route(
				'gital',
				'/display_ads/fetch_available_ads',
				array(
					'methods'             => 'GET',
					'callback'            => array( $this, 'fetch_available_ads_callback' ),
					'permission_callback' => '__return_true',
				)
			);
		}

		/**
		 * Returns the image component
		 *
		 * @param int $image_id The id of the image used here.
		 * @param string $class The class of the image.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		private function create_image( $image_id, $class ) {
			$dom = new \DOMDocument();

			$component = $dom->createElement( 'img' );

			$component->setAttribute( 'class', $class );
			$component->setAttribute( 'src', esc_url( wp_get_attachment_image_url( $image_id, 'large' ) ) );
			$component->setAttribute( 'srcset', esc_attr( wp_get_attachment_image_srcset( $image_id, 'large' ) ) );
			$component->setAttribute( 'sizes', esc_attr( wp_get_attachment_image_sizes( $image_id, array( '1920', '1920' ) ) ) );

			$image_intermediate_size = image_get_intermediate_size( $image_id, 'large' );

			if ( $image_intermediate_size ) {
				$component->setAttribute( 'width', esc_attr( $image_intermediate_size['width'] ) );
				$component->setAttribute( 'height', esc_attr( $image_intermediate_size['height'] ) );
			}

			$alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
			if ( ! empty( $alt ) ) {
				$component->setAttribute( 'alt', $alt );
			}

			return $component;
		}

		/**
		 * The callback method that returns an ad.
		 *
		 * @param Array $payload The REST-api payload.
		 *
		 * @return string The HTML of the ad.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fetch_ad_callback( $payload ) {
			$parameters = $payload->get_params();
			$dom        = new \DOMDocument();

			$id = $parameters['id'] ?? false;

			if ( ! $id ) {
				return;
			}

			$desktop_image_id = get_field( 'display_ad_desktop', $id );
			$mobile_image_id  = get_field( 'display_ad_mobile', $id );
			$link             = get_field( 'link', $id );

			$component = $dom->createElement( ( ! empty( $link ) ? 'a' : 'div' ) );
			$component->setAttribute( 'class', 'g-da-display-ads__inner' );

			if ( ! empty( $link ) ) {
				$component->setAttribute( 'href', $link );
				$component->setAttribute( 'target', '_blank' );
				$component->setAttribute( 'title', get_the_title( $id ) );
				$component->setAttribute( 'rel', 'noopener' );
			}

			$component->appendChild( $dom->importNode( self::create_image( $desktop_image_id, 'g-da-display-ads__image g-da-display-ads__image--desktop' ), true ) );
			$component->appendChild( $dom->importNode( self::create_image( $mobile_image_id, 'g-da-display-ads__image g-da-display-ads__image--mobile' ), true ) );

			$dom->appendChild( $component );

			return $dom->saveHTML();
		}


		/**
		 * The callback method that returns all the ads ids.
		 *
		 * @return Array An array of ad ids.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function fetch_available_ads_callback() {
			$all_ads = get_posts(
				array(
					'fields'         => 'ids',
					'post_type'      => 'display_ads',
					'post_status'    => 'publish',
					'posts_per_page' => -1,
				)
			);
			return $all_ads;
		}
	}
}
